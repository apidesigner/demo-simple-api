# Simple API Demo
Demo of a Simple API including CI/CD tests and Apiary integration. 

This is the Bitbucket version of <https://github.com/adidas-group/demo-simple-api>.

## API Documentation
See the documentation at [Apiary.io](http://docs.demosimpleapibitbucket.apiary.io/#)

## Tests
The API Description contract (swagger.yaml) is used to test the implementation (sever.js) with the [Dredd](https://github.com/apiaryio/dredd) CLI testing tool.

The tests can be run locally by typing `dredd` in the project directory (requires a Node.js and Dredd installation).

This repository has a CI/CD pipeline configured. The pipelines works as follows:

1. Install server dependencies
2. Install Dredd testing tool
3. Run Dredd tests
4. If all tests passsed, install Apiary CLI tool and push the swagger.yaml file into Apiary

See this project's Bitbucket pipeline configuration (bitbucket-pipelines) for details.

The builds are available at https://bitbucket.org/apidesigner/demo-simple-api/addon/pipelines/home#!/.
